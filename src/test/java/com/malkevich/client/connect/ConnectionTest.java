package com.malkevich.client.connect;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * Created by Stepan on 24.04.16.
 */
public class ConnectionTest {

    Connection connection;

    @Before
    public void getInstance() throws ClassNotFoundException, IOException, IllegalAccessException, InstantiationException {
        String name = Connection.class.getName();
        connection = (Connection) Connection.class.getClassLoader().loadClass(name).newInstance();
    }

    @Test
    public void should_correct_Plus() throws Exception {
        assertEquals("23.0", connection.getResult("2 + 2 + 3 + 16"));
    }

    @Test
    public void should_correct_Minus() throws Exception {
        assertEquals("15.0", connection.getResult("20 - 10 + 5"));
    }

    @Test
    public void should_correct_Mult() throws Exception {
        assertEquals("15.0", connection.getResult("20 - 10 + 5"));
    }

    @Test
    public void should_correct_Div() throws Exception {
        assertEquals("10.0", connection.getResult("100/10"));
    }

    @Test
    public void should_correct_Pow() throws Exception {
        assertEquals("1024.0", connection.getResult("2 ^ 10"));
    }

    @Test
    public void should_correct_all_hardestInAllUniverse() throws Exception {
        assertEquals("-2422.867351748293", connection.getResult("(sin(5)*abs(30 + 26*100 - 70/12) + 2^(2 + 3 * 2) + 3 - 4 * 10/5-(20 * 8) + cos(10) - 200/43 + 8 )"));
    }


}