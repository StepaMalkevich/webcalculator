package com.malkevich.client.connect;

import com.malkevich.client.application.CalculatorFrame;
import org.fest.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by Stepan on 24.04.16.
 */
public class CalculatorFrameTest {

    private FrameFixture calculator;

    @Before
    public void setUp() throws IOException {
        calculator = new FrameFixture(new CalculatorFrame());
    }

    @After
    public void tearDown() {
        calculator.cleanUp();
    }

    @Test
    public void should_correct_Plus() {
        calculator.textBox("input").enterText("2+2+100+1000");
        calculator.button("compute").click();
        calculator.label("output").requireText("Result: 1104.0");
    }

    @Test
    public void should_correct_Minus() {
        calculator.textBox("input").enterText("256 - 239");
        calculator.button("compute").click();
        calculator.label("output").requireText("Result: 17.0");
    }

    @Test
    public void should_correct_Mult() {
        calculator.textBox("input").enterText("1*(1*(1*1)*1)*25 * 25");
        calculator.button("compute").click();
        calculator.label("output").requireText("Result: 625.0");
    }

    @Test
    public void should_correct_Div() {
        calculator.textBox("input").enterText("36 / 6 * 6");
        calculator.button("compute").click();
        calculator.label("output").requireText("Result: 36.0");
    }

    @Test
    public void should_correct_Pow() {
        calculator.textBox("input").enterText("2^2 + 2^2 + 2^2");
        calculator.button("compute").click();
        calculator.label("output").requireText("Result: 12.0");
    }

    @Test
    public void should_correct_All() {
        calculator.textBox("input").enterText("(sin(5)*abs(30 + 26*100 - 70/12) + 2^(2 + 3 * 2) + 3 - 4 * 10/5-(20 * 8) + cos(10) - 200/43 + 8 )");
        calculator.button("compute").click();
        calculator.label("output").requireText("Result: -2422.867351748293");
    }
}