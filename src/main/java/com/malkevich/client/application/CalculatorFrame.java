package com.malkevich.client.application;

import com.malkevich.client.connect.Connection;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.ConnectException;

/**
 * Created by Stepan on 24.04.16.
 */
public class CalculatorFrame extends JFrame implements ActionListener {
    private JTextField inputtextField;
    private JLabel outputLabel;

    public CalculatorFrame() throws IOException {
        this.setContentPane(new JLabel(new ImageIcon(ImageIO.read(new File("src/main/java/com/malkevich/server/utilities/MalkevichCalculator.jpg")))));

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setBounds(100, 100, 450, 300);
        getContentPane().setLayout(null);

        JButton buttonToCompute = new JButton("Compute");
        buttonToCompute.setName("compute");
        buttonToCompute.addActionListener(this);

        buttonToCompute.setBounds(698, 246, 71, 45);
        getContentPane().add(buttonToCompute);

        inputtextField = new JTextField();
        inputtextField.setBounds(254, 251, 450, 35);
        getContentPane().add(inputtextField);
        inputtextField.setColumns(20);
        inputtextField.setName("input");

        outputLabel = new JLabel("Result: ");
        outputLabel.setBounds(257, 290, 500, 14);
        getContentPane().add(outputLabel);
        outputLabel.setName("output");

        this.setResizable(false);
        this.pack();
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String expression = inputtextField.getText();
        Connection connection = new Connection();
        String answer;
        try {
            answer = connection.getResult(expression);
        } catch (ConnectException e1) {
            answer = "Server is not connected. Please first start the server";
        } catch (IOException e1) {
            answer = "The format of your expression is incorrect. Please enter a valid query";
        }

        outputLabel.setText("Result: " + answer);
    }
}
