package com.malkevich.client.connect;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Stepan on 24.04.16.
 */
public interface LocalhostClient {
    @GET("/api/calculate/{expression}")
    Call<String> calculate(@Path("expression") final String expression);
}
