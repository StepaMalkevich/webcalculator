package com.malkevich.server.model.lexer;

import java.util.Deque;

/**
 * Created by Stepan on 23.04.16.
 */
public interface Calculable {
    double getResult(Deque<Token> tokens);
}
