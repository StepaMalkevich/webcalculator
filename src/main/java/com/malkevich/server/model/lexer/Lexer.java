package com.malkevich.server.model.lexer;

import java.util.Deque;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Stepan on 23.04.16.
 */
public class Lexer {
    public static Deque<Token> useLexer(final String expression) {
        return lex(expression);
    }

    private static Deque<Token> lex(final String input) {
        Deque<Token> tokens = new LinkedList<>();

        StringBuilder tokenPatternsBuilder = new StringBuilder();
        for (TokenType tokenType : TokenType.values()) {
            tokenPatternsBuilder.append(String.format("|(?<%s>%s)", tokenType.name(), tokenType.getPattern()));
        }

        Pattern tokenPatterns = Pattern.compile(tokenPatternsBuilder.substring(1));

        Matcher matcher = tokenPatterns.matcher(input);
        while (matcher.find()) {
            for (int i = 0; i < TokenType.values().length - 1; i++) {
                addTokens(matcher, tokens, TokenType.values()[i]);
            }
        }

        return tokens;
    }

    private static void addTokens(final Matcher matcher, final Deque<Token> tokens, final TokenType tokenType) {
        if (matcher.group(tokenType.name()) != null) {
            tokens.offer(new Token(tokenType, matcher.group(tokenType.name())));
        }
    }
}
