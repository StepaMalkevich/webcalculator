package com.malkevich.server.model.lexer;

import com.malkevich.server.model.parser.Parser;

import java.util.Deque;

/**
 * Created by Stepan on 23.04.16.
 */
public enum TokenType implements Calculable {
    DOUBLE("[0-9]+(\\.[0-9]+)?(e[\\+-]?[0-9]+)?") {
        public double getResult(final Deque<Token> tokens) {
            return tokens.poll().value();
        }
    },
    PLUS("[+]") {
        public double getResult(final Deque<Token> tokens) {
            return calcPlusMinus(tokens);
        }
    },
    MINUS("[-]") {
        public double getResult(final Deque<Token> tokens) {
            return -calcPlusMinus(tokens);
        }
    },
    MULT("[*]") {
        public double getResult(final Deque<Token> tokens) {
            return 0;
        }
    },
    DIV("[/]") {
        public double getResult(final Deque<Token> tokens) {
            return 0;
        }
    },
    POW("[\\^]") {
        public double getResult(final Deque<Token> tokens) {
            return 0;
        }
    },
    LEFTBRACKET("[(]") {
        public double getResult(final Deque<Token> tokens) {
            tokens.poll();
            double result = Parser.calcExpr(tokens);
            tokens.poll();
            return result;
        }
    },
    RIGHTBRACKET("[)]") {
        public double getResult(final Deque<Token> tokens) {
            return 0;
        }
    },
    SIN("sin") {
        public double getResult(final Deque<Token> tokens) {
            return calcFunc(tokens);
        }
    },
    COS("cos") {
        public double getResult(final Deque<Token> tokens) {
            return calcFunc(tokens);
        }
    },
    ABS("abs") {
        public double getResult(final Deque<Token> tokens) {
            return calcFunc(tokens);
        }
    },
    WHITESPACE("[ \t\f\r\n]+") {
        public double getResult(final Deque<Token> tokens) {
            return 0;
        }
    };

    private static double calcPlusMinus(final Deque<Token> tokens) {
        tokens.poll();
        return Parser.calcFact(tokens);
    }

    public double calcFunc(final Deque<Token> tokens) {
        tokens.poll();
        tokens.poll();
        double result = 0;
        switch (this) {
            case SIN:
                result = Math.sin(Parser.calcExpr(tokens));
                break;
            case COS:
                result = Math.cos(Parser.calcExpr(tokens));
                break;
            case ABS:
                result = Math.abs(Parser.calcExpr(tokens));
                break;
            default:
                throw new IllegalArgumentException();
        }
        tokens.poll();
        return result;
    }

    private final String pattern;

    public String getPattern() {
        return pattern;
    }

    TokenType(final String pattern) {
        this.pattern = pattern;
    }
}
