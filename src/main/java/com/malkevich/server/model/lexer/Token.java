package com.malkevich.server.model.lexer;

/**
 * Created by Stepan on 23.04.16.
 */
public class Token {
    private final TokenType type;
    private final String data;

    public Token(final TokenType type, final String data) {
        this.type = type;
        this.data = data;
    }

    public TokenType getType() {
        return type;
    }

    public double value() {
        return Double.parseDouble(data);
    }

    @Override
    public String toString() {
        return String.format("(%s %s)", type.name(), data);
    }

}
