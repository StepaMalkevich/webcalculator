package com.malkevich.server.model.parser;

import com.malkevich.server.model.lexer.Calculable;
import com.malkevich.server.model.lexer.Token;
import com.malkevich.server.model.lexer.TokenType;

import java.util.Deque;

/**
 * Created by Stepan on 23.04.16.
 */
public class Parser {
    public static double parse(final Deque<Token> tokens) {
        return calcExpr(tokens);
    }

    /*
    E -> T | T + E | T - E
    */
    public static double calcExpr(final Deque<Token> tokens) {
        double result = calcTerm(tokens);

        if (tokens.isEmpty()) {
            return result;
        }

        while (tokens.peek().getType() == TokenType.PLUS || tokens.peek().getType() == TokenType.MINUS) {
            switch (tokens.poll().getType()) {
                case PLUS:
                    result = result + calcTerm(tokens);
                    break;
                case MINUS:
                    result = result - calcTerm(tokens);
                    break;
                default:
                    throw new IllegalArgumentException();
            }
            if (tokens.isEmpty()) {
                return result;
            }

        }
        return result;
    }

    /*
    T-> F | F * T | F / T
    */
    public static double calcTerm(final Deque<Token> tokens) {
        double result = calcFact(tokens);

        if (tokens.isEmpty()) {
            return result;
        }

        while (tokens.peek().getType() == TokenType.MULT || tokens.peek().getType() == TokenType.DIV) {
            switch (tokens.poll().getType()) {
                case MULT:
                    result = result * calcFact(tokens);
                    break;
                case DIV:
                    result = result / calcFact(tokens);
                    break;
                default:
                    throw new IllegalArgumentException();
            }
            if (tokens.isEmpty()) {
                return result;
            }
        }
        return result;
    }

    /*
    F-> D | D^F
    */
    public static double calcFact(final Deque<Token> tokens) {
        double result = calcNum(tokens);

        if (tokens.isEmpty()) {
            return result;
        }

        while (tokens.peek().getType() == TokenType.POW) {
            tokens.poll();
            result = Math.pow(result, calcFact(tokens));
            if (tokens.isEmpty()) {
                return result;
            }
        }
        return result;
    }

    /*
    D-> double | (E) | sin(E) | cos(E) | abs(E)
    */
    public static double calcNum(final Deque<Token> tokens) {
        double result = 0;

        if (tokens.isEmpty()) {
            return result;
        }

        return useRightRule(tokens, tokens.peek().getType());
    }

    private static double useRightRule(final Deque<Token> tokens, final Calculable calculable) {
        return calculable.getResult(tokens);
    }
}
