package com.malkevich.server.rest;

import com.malkevich.server.model.lexer.Lexer;
import com.malkevich.server.model.lexer.Token;
import com.malkevich.server.model.parser.Parser;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.util.Deque;

/**
 * Created by Stepan on 23.04.16.
 */
@Path("/calculate")
public class CalculatorApi {
    @Path("/{expression}")
    @GET
    public String calculateExpression(@PathParam("expression") final String expressionToGet) {
        String expression = expressionToGet.replaceAll("@@@", "/");
        Double answer = calculate(expression);
        return String.valueOf(answer);
    }

    private double calculate(final String expression) {
        String clearExpression = expression.toLowerCase();
        Deque<Token> tokens = Lexer.useLexer(clearExpression);
        return Parser.parse(tokens);
    }
}
