# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* MalkevichCalculator version 1.0
* Functionality: (()),(+), (-), (*), (/), (^), abs, sin, cos

### How do I get set up? ###

* Download Tomcat
* Run Tomcat and server will be connected
* Run Application class and calculator will be started 

### Test guidelines ###

* Run ConnectionTest class to make sure that connection work correctly
* Run CalculatorFrameTest class to make sure that all application(connection and client interface) work correctly
* You must have the English keyboard layout before running CalculatorFrameTest class